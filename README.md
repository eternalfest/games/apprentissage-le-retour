# Apprentissage : le retour

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/apprentissage-le-retour.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/apprentissage-le-retour.git
```
